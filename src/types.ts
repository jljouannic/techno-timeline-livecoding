
export interface Node {
  id: string
  name: string
  date: Date
  color: string
  logo: string
}

export interface Link {
  source: Node
  target: Node
}

export interface Data {
  nodes: Node[]
  links: Link[]
}
